var numberArr = [];
function themSo() {
  var numberValue = document.getElementById("txt-number").value * 1;
  numberArr.push(numberValue);
  document.getElementById("txt-ket-qua").innerHTML = numberArr;
}
console.log(numberArr);
// bài 1
/* 
input:Giá trị phần tử của mảng

Các bước xử lý;
-cho biên Tổng bằng 0
- Sử dụng vòng lặp for
+ Nếu giá trị phần tử lớn hơn 0 thì cộng vào biến Tổng

Output:
Giá trị biến Tổng
*/
function tongSoDuong() {
  var sumPosNumber = 0;
  for (var i = 0; i < numberArr.length; i++) {
    var currentValue = numberArr[i] * 1;
    if (currentValue > 0) {
      sumPosNumber += currentValue;
    }
  }
  document.getElementById("ket-qua-bai-1").innerHTML =
    `Tổng số dương : ` + sumPosNumber;
}
// bai 2
/**
Input:Giá trị phần tử của mảng

Các bước xử lý;
- cho biến Đếm = 0
- dùng vòng lặp for
+nếu giá trị phần tử > 0 thì biến Đếm ++

Output:
Giá trị biến đếm
 */
function demSoDuong() {
  var countPosNumber = 0;
  for (var i = 0; i < numberArr.length; i++) {
    var currentValue = numberArr[i] * 1;
    if (currentValue > 0) {
      countPosNumber++;
    }
    document.getElementById("ket-qua-bai-2").innerHTML =
      ` Số Dương : ` + countPosNumber;
  }
}
// bai 3
/**
Input:Giá trị phần tử của mảng

Các bước xử lý:
-Cho biến Min = phần tử đầu tiên a[0]
-dùng vòng lặp for
+nếu a[i] > Min thì Min mới = a[i];

Output:
Giá trị min
 */

function timSoNhoNhat() {
  for (var i = 0; i < numberArr.length; i++) {
    minValue = numberArr[0] * 1;
    if (numberArr[i] < minValue) {
      minValue = numberArr[i];
    }
    document.getElementById("ket-qua-bai-3").innerHTML =
      ` Số Nhỏ Nhất : ` + minValue;
  }
}
/**
Input:Giá trị phần tử của mảng

Các bước xử lý:
-Lấy những phần tử dương của mảng cũ sang mảng mới

-Xem trong mảng mới có phần tử ko

+Nếu không thì in "Không có số dương nào"

+Nếu có: cho biến số dương nhỏ nhất = posArr[0];
duyệt qua các phần tử của mảng
nếu posArr[i]<minPosVale thì chọn làm min mới

Output:
Số dương nhỏ nhất
 */
// bài 4
function timSoDuongNhoNhat() {
  var posArr = [];
  for (var i = 0; i < numberArr.length; i++) {
    var currentValue = numberArr[i] * 1;
    if (currentValue > 0) {
      var posNumberValue = currentValue;
      posArr.push(posNumberValue);
    }
  }
  if (posArr.length != 0) {
    var minPosValue = posArr[0];
    for (var i = 0; i < posArr.length; i++) {
      if (posArr[i] < minPosValue) {
        minPosValue = posArr[i];
      }
    }
    document.getElementById("ket-qua-bai-4").innerHTML =
      ` Số Dương Nhỏ Nhất : ` + minPosValue;
  } else {
    document.getElementById(
      "ket-qua-bai-4"
    ).innerHTML = ` Không có số dương nào`;
  }
}
// bai 5
/**
Input:Giá trị các phần tử trong mảng

Các bước xử lý:
-dùng vòng lặp for
+ nếu giá trị ptu % 2 == 0 thì gắn bằng Số chẵn cuối cùng
=> Hết vòng for thì sẽ có số chẫn cuối cùng

Output:
Số chẵn cuối cùng
 */
function timSoChanCuoiCung() {
  var lastPosValue;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] % 2 == 0) {
      lastPosValue = numberArr[i];
    }
  }
  document.getElementById("ket-qua-bai-5").innerHTML =
    `Số chẵn cuối cùng trong mảng : ` + lastPosValue;
}
// bai 6
/**
Input:
-Giá trị các phần tử trong mảng
-Hai vị trí muốn đổi chỗ

Các bước xử lý:
- Cho biến value tạm thời = giá trị thứ nhất
           gán phần tử thứ nhất = giá trị phần tử thứ 2
           gán phần tử thứ hai = giá trị value tạm thời;
           => đổi chỗ bằng cách dùng biến chung để đổi giá trị 
Output:
In ra mảng sau khi đổi chỗ
 */
function doiCho() {
  var viTriThuNhat = document.getElementById("txt-so-1").value;
  var viTriThuHai = document.getElementById("txt-so-2").value;
  var doiChoValue = numberArr[viTriThuNhat];
  numberArr[viTriThuNhat] = numberArr[viTriThuHai];
  numberArr[viTriThuHai] = doiChoValue;
  document.getElementById("ket-qua-bai-6").innerHTML = numberArr;
}
// pos = a
// a = b
// b = pos
// bài 7
/**
Input:giá trị phần tử trong mảng

Các bước xử lý:
-Dùng 2 vòng lặp for để sắp xếp
+ cho j = i+1.Nếu phần tử trước lớn phần tử kế tiếp  thì đổi chỗ 2 phần tử
+ dùng biến tạm thời để đổi chỗ 2 phần tử
 
Output:
Mảng theo giá trị tăng dần
 */
function sapXepTangDan() {
  var temp;
  for (var i = 0; i < numberArr.length - 1; i++) {
    for (var j = i + 1; j < numberArr.length; j++) {
      if (numberArr[i] > numberArr[j]) {
        temp = numberArr[i];
        numberArr[i] = numberArr[j];
        numberArr[j] = temp;
      }
    }
  }
  console.log(numberArr);
  document.getElementById("ket-qua-bai-7").innerHTML = numberArr;
}
//bài 8
/** 
Input:giá trị các phần tử trong mảng

Các bước xử lý:
-Xác định số nguyên tố:
*Một số không phải là số ngto khi:
- số bé hơn 2
- nếu số lớn hơn bằng 2: 
Chia hết cho những số từ 2 cho đến căn của chính nó;
=>Những số còn lại là số nguyên tố

Output:
Nếu có SNT thì in ra số ngto
Nếu không thì in ra -1
*/
function xacDinhSNT(value) {
  if (value < 2) {
    return false;
  }
  if (value >= 2) {
    for (var i = 2; i <= Math.sqrt(value); i++) {
      if (value % i == 0) {
        return false;
      }
    }
    return true;
  }
}

function timSNTdauTien() {
  for (var i = 0; i < numberArr.length; i++) {
    if (xacDinhSNT(numberArr[i])) {
      document.getElementById("ket-qua-bai-8").innerHTML = numberArr[i];
      break;
    } else {
      document.getElementById("ket-qua-bai-8").innerHTML = "-1";
    }
  }
}
// bài 9
/**
Input:Giá trị các phần tử của mạng mới

Các bước xử lý:
- Xác định số nguyên khi:
Giá trị làm tròn lên = giá trị làm tròn xuống của nó;
=> sử dụng hàm làm tròn lên Math.ceil và tròn xuống Math.floor 
-cho biến đếm = 0
+ Nếu phần tử là số nguyên thì đếm ++

Output:
In ra biến đếm
 */
var numberArr2 = [];
function themSo2() {
  var numberValue2 = document.getElementById("txt-number2").value;
  numberArr2.push(numberValue2);
  document.getElementById("txt-number2").value = "";
  document.getElementById("txt-ket-qua2").innerHTML = numberArr2;
  for (var i = 0; i < numberArr2.length; i++) {
    current2 = numberArr2[i];
    console.log(current2);
  }
}
function demSoNguyen() {
  var dem = 0;
  for (var i = 0; i < numberArr2.length; i++) {
    if (Math.floor(numberArr2[i]) == Math.ceil(numberArr2[i])) {
      dem++;
    }
  }
  document.getElementById("ket-qua-bai-9").innerHTML = ` Số nguyên :` + dem;
}
// bài 10
/**
Input:Giá trị các phần tử trong mảng

Các bước xử lý:
-Cho 2 biến đếm âm dương =0
-Nếu giá trị phần tử dương thì biến dương ++ 
ko thì biến âm ++
-dùng if (dương > âm),(dương < âm),(dương = âm)

Output:
Kết quả so sánh
 */
function soSanhAmDuong() {
  var negCount = 0,
    posCount = 0;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] > 0) {
      posCount++;
    } else if (numberArr[i] < 0) {
      negCount++;
    }
  }
  if (posCount > negCount) {
    document.getElementById("ket-qua-bai-10").innerHTML = ` Số dương > số âm`;
  } else if (posCount < negCount) {
    document.getElementById("ket-qua-bai-10").innerHTML = ` Số dương < số âm`;
  } else {
    document.getElementById("ket-qua-bai-10").innerHTML = ` Số dương = số âm`;
  }
}
